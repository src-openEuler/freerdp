Name:           freerdp
Version:        2.11.7
Release:        6
Epoch:          2
Summary:        A Remote Desktop Protocol Implementation
License:        Apache-2.0
URL:            https://www.freerdp.com

Source0:        https://github.com/FreeRDP/FreeRDP/releases/download/%{version}/freerdp-%{version}.tar.gz
Patch0001:	Fix-freerdp-shadow-cli-exit-codes-for-help-and-version.patch
Patch0002:	CVE-2024-32661.patch
Patch0003:	Fix-clang-incompatible-function-pointer-error.patch
Patch0004:      freerdp-2.11.7-port-to-newer-cmake.patch

BuildRequires:  gcc gcc-c++ alsa-lib-devel cmake >= 2.8 cups-devel gsm-devel libXrandr-devel libXv-devel
BuildRequires:  libjpeg-turbo-devel libjpeg-turbo-devel libX11-devel libXcursor-devel libxkbfile-devel
BuildRequires:  libXdamage-devel libXext-devel libXi-devel libXinerama-devel libxkbfile-devel pam-devel
BuildRequires:  xmlto zlib-devel pkgconfig(dbus-1) pkgconfig(dbus-glib-1) pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(glib-2.0) pkgconfig(gstreamer-base-1.0) pkgconfig(gstreamer-app-1.0)
BuildRequires:  pkgconfig(gstreamer-audio-1.0) pkgconfig(gstreamer-fft-1.0) pkgconfig(gstreamer-video-1.0)
BuildRequires:  pkgconfig(gstreamer-pbutils-1.0) pkgconfig(libpcsclite) pkgconfig(libpulse)
BuildRequires:  pkgconfig(libpulse) pkgconfig(libsystemd) pkgconfig(wayland-scanner)
BuildRequires:  pkgconfig(xkbcommon) pkgconfig(openssl) pkgconfig(wayland-client) pkgconfig(cairo)
BuildRequires:  pkgconfig(libusb-1.0)

Requires:       libwinpr = %{?epoch}:%{version}-%{release} systemd-pam
Provides:       %{name}-libs = %{?epoch}:%{version}-%{release} xfreerdp = %{version}-%{release}
Provides:       %{name}-plugins = %{?epoch}:%{version}-%{release}
Provides:       %{name}-server = %{?epoch}:%{version}-%{release}
Obsoletes:      %{name}-server < %{?epoch}:%{version}-%{release}  %{name}-plugins < 1:1.1.0
Obsoletes:      %{name}-libs < %{?epoch}:%{version}-%{release}

%description
FreeRDP is a client implementation of the Remote Desktop Protocol (RDP) that follows Microsoft's
open specifications. This package provides the client applications xfreerdp and wlfreerdp.

%package        devel
Summary:        Development support for freerdp
Requires:       %{name} = %{?epoch}:%{version}-%{release} pkgconfig cmake >= 2.8

%description    devel
Development headers and libraries for freerdp-libs.

%package -n     libwinpr
Summary:        Windows Portable Runtime
Provides:       %{name}-libwinpr = %{?epoch}:%{version}-%{release}
Obsoletes:      %{name}-libwinpr < %{?epoch}:%{version}-%{release}

%description -n libwinpr
WinPR provides API compatibility for non-Windows applications surroundings.
On Windows, you can use the native API and use it to change the code.

%package -n     libwinpr-devel
Summary:        Windows Portable Runtime development headers and libraries
Requires:       libwinpr = %{?epoch}:%{version}-%{release}

%description -n libwinpr-devel
Development headers and libraries for freerdp-libwinpr.

%package_help

%prep
%autosetup -p1 -n %{name}-%{version}

find . -name "*.h" -exec chmod 664 {} \;
find . -name "*.c" -exec chmod 664 {} \;
%build
%cmake %{?_cmake_skip_rpath} \
    -DCMAKE_INSTALL_LIBDIR:PATH=%{_lib} -DWITH_ALSA=ON -DWITH_CUPS=ON -DWITH_CHANNELS=ON \
    -DBUILTIN_CHANNELS=OFF -DWITH_CLIENT=ON -DWITH_DIRECTFB=OFF -DWITH_FFMPEG=OFF -DWITH_GSM=ON \
    -DWITH_GSSAPI=OFF -DWITH_GSTREAMER_1_0=ON -DWITH_GSTREAMER_0_10=OFF \
    -DPROXY_PLUGINDIR=%{_libdir}/freerdp2/server/proxy/plugins \
    -DGSTREAMER_1_0_INCLUDE_DIRS=%{_includedir}/gstreamer-1.0 -DWITH_IPP=OFF -DWITH_JPEG=ON \
    -DWITH_MANPAGES=ON DWITH_OPENH264=OFF \
    -DWITH_OPENSSL=ON -DWITH_PCSC=ON -DWITH_PULSE=ON -DWITH_SERVER=ON -DWITH_SERVER_INTERFACE=ON \
    -DWITH_SHADOW_X11=ON -DWITH_SHADOW_MAC=ON -DWITH_WAYLAND=ON -DWITH_X11=ON \
    -DWITH_X264=OFF -DWITH_XCURSOR=ON -DWITH_XEXT=ON \
    -DWITH_XKBFILE=ON -DWITH_XI=ON -DWITH_XINERAMA=ON -DWITH_XRENDER=ON -DWITH_XTEST=OFF \
    -DWITH_XV=ON -DWITH_ZLIB=ON \
%ifarch x86_64
    -DWITH_SSE2=ON \
%else
    -DWITH_SSE2=OFF \
%endif

%cmake_build

%install
%cmake_install

%files
%license LICENSE
%doc README.md ChangeLog
%dir %{_libdir}/freerdp2/
%{_libdir}/freerdp2/*.so
%{_bindir}/{winpr-hash,winpr-makecert,wlfreerdp,xfreerdp,freerdp-shadow-cli,freerdp-proxy}
%{_libdir}/{libfreerdp*,libuwac0}.so.*

%files devel
%{_includedir}/{freerdp2,uwac0}
%{_libdir}/cmake/{FreeRDP*,uwac0}
%{_libdir}/{libfreerdp*,libuwac0}.so
%{_libdir}/pkgconfig/{freerdp*,uwac0}.pc

%files -n libwinpr
%license LICENSE
%doc README.md ChangeLog
%{_libdir}/{libwinpr2.so.*,libwinpr-tools2.so.*}

%files -n libwinpr-devel
%{_libdir}/cmake/WinPR2
%{_includedir}/winpr2
%{_libdir}/{libwinpr2,libwinpr-tools2}.so
%{_libdir}/pkgconfig/{winpr2,winpr-tools2}.pc

%files help
%{_mandir}/man?/*

%changelog
* Thu Mar 06 2025 Funda Wang <fundawang@yeah.net> - 2:2.11.7-6
- build with cmake 4.0

* Tue Jan 28 2025 Funda Wang <fundawang@yeah.net> - 2:2.11.7-5
- fix build with cmake macro migration

* Mon Nov 18 2024 Funda Wang <fundawang@yeah.net> - 2:2.11.7-4
- adopt to new cmake macro

* Mon Aug 05 2024 yanying <201250106@smail.nju.edu.cn> - 2:2.11.7-3
- Fix clang incompatible function pointer error

* Mon May 06 2024 wangkai <13474090681@163.com> - 2:2.11.7-2
- Fix CVE-2024-32661

* Tue Apr 23 2024 wangkai <13474090681@163.com> - 2:2.11.7-1
- Update to 2.11.7 for fix CVE-2024-32039,CVE-2024-32040,
  CVE-2024-32041,CVE-2024-32458,CVE-2024-32459,CVE-2024-32460,
  CVE-2024-32658,CVE-2024-32659 and CVE-2024-32660

* Wed Jan 24 2024 wangkai <13474090681@163.com> - 2:2.11.1-2
- Fix CVE-2024-22211

* Wed Sep 06 2023 Funda Wang <fundawang@yeah.net> - 2:2.11.1-1
- 2.11.1

* Sun Apr 23 2023 liyanan <thistleslyn@163.com> - 2:2.10.0-1
- upgrade 2.10.0

* Tue Dec 13 2022 liyanan <liyanan32@h-partners.com>  - 2:2.8.1-4
- add requires systemd-pam

* Fri Nov 25 2022 liyuxiang <liyuxiang@ncti-gba.cn> - 2:2.8.1-3
- Fix CVE-2022-39320
- Fix CVE-2022-39317

* Tue Nov 22 2022 liyuxiang <liyuxiang@ncti-gba.cn> - 2:2.8.1-2
- Fix CVE-2022-39316
- Fix CVE-2022-39318
- Fix CVE-2022-39319
- Fix CVE-2022-39347
- Fix CVE-2022-41877

* Thu Oct 20 2022 jiangpeng <jiangpeng01@ncti-gba.cn> - 2:2.8.1-1
- Upgrade to 2.8.1
- Fix CVE-2022-39282

* Sat May 07 2022 houyingchao <houyingchao@h-partners.com> - 2:2.7.0-1
- Upgrade to 2.7.0
- Fix CVE-2022-24882 CVE-2022-24883

* Mon Nov 08 2021 lingsheng <lingsheng@huawei.com> - 2:2.4.1-2
- Add ldconfig config file to fix freerdp-proxy dynamic library dependency

* Tue Nov 2 2021 yaoxin <yaoxin30@huawei.com> - 2:2.4.1-1
- Upgrade freerdp to 2.4.1 for fix CVE-2021-41159, CVE-2021-41160

* Wed Jan 27 2021 sunguoshuai <sunguoshuai@huawei.com> - 2:2.2.0-2
- Fix xfreerdp and free-rdpshadow-cli exit codes for help and similar option

* Wed Jan 6 2021 zhanghua <zhanghua40@huawei.com> - 2:2.2.0-1
- Type: cves
- ID: NA
- SUG: NA
- DESC: update to 2.2.0 to fix cves, fix changelog version

* Wed May 20 2020 maqiang <maqiang42@huawei.com> - 2:2.0.0-45.rc3
- Change release

* Wed Nov 20 2019 duyeyu <duyeyu@huawei.com> - 2:2.0.0-44.rc3
- Package init
